﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipAndTowBackend.Processing
{
    public class utils
    {
        public struct alertMessage
        {
            // SDD-SHR-0010001
            public string messageColor;
            public bool multiLine;
            public string icon;
            public string message;
            public int timeout;
            public string avatar;
            public string position;
            public string type;

            // Syntax Explaination (New Developer Tutorial):
            //  This is a "constructor" for the alertMessage structure
            //  This is optional, but allows us to easily create a new alertMessage with the
            //      default values already provided, similar to calling a function - As opposed
            //      to declaring a new alertMessage structure with each value defined.

            // Default message types:
            //  'positive', 'negative', 'warning', 'info'
            // Specifying a message type will set the default color and icon
            public alertMessage(string MESSAGE, int TIMEOUT = 0, string TYPE = "warning", string MESSAGECOLOR = "", string ICON = "", bool MULTILINE = false, string AVATAR = "", string POSITION = "top")
            {
                message = MESSAGE;
                type = TYPE;
                timeout = TIMEOUT;
                messageColor = MESSAGECOLOR;
                multiLine = MULTILINE;
                icon = ICON;
                avatar = AVATAR;
                position = POSITION;
            }
        }
    }
}
