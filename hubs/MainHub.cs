﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using ShipAndTowBackend.Processing;

namespace ShipAndTowBackend.hubs
{
    public class MainHub : Hub
    {
        accountManagement accountManagement = new accountManagement();
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            // Client has disconnected from the server hub
            await base.OnDisconnectedAsync(exception);
        }

        // System Testing - Management
        public async Task testError(string message)
        {
            // This is an example of basic client -> server communication
            //  The client will invoke the testError() method via websocket with a message
            //  The server will pack the client's string into an alertMessage object and send it back
            //  The client will parse and display the alertMessage object with the specifid attributes

            // Build an alertMessage response - 5000ms auto dismiss
            utils.alertMessage response = new utils.alertMessage(message, 5000, "info");

            // Send the alertMessage back to the calling client
            await Clients.Caller.SendAsync("returnAlertMessage", response);
        }

        // [SRD-BCK-002XXXX] Account Management
        public async Task createAccount(accountManagement.registerAccountInfo accRegisterData)
        {
            // TODO
            // SRD-BCK-0201001
            (bool, string) result_create = accountManagement.createAccount(accRegisterData);

            await Clients.Caller.SendAsync("ackCreateAccount", "TODO");
        }
    }
}
