using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using System.Net;
using ShipAndTowBackend.hubs;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;

namespace ShipAndTowBackend
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Cross-Origin Policy
            services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
            {
                // Here we currently allow requests from all origins
                //  Mobile or Desktop applications will allow this user to make requests
                //    from their device's IP, as oppsed to only from our web domain.
                builder
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .SetIsOriginAllowed(_ => true)
                    .AllowCredentials();
            }));

            // SignalR Service
            services.AddSignalR().AddNewtonsoftJsonProtocol();

            // SignalR Hub Services
            //services.AddScoped<MainHub>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Serve Static Files in wwwroot
            app.UseStaticFiles();

            // Forward headers from Apache to Kestrel Server - Allows us to retrieve the origional IP
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                ForwardLimit = 2
            });

            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                // SignalR Websocket Hub Endpoints
                endpoints.MapHub<MainHub>("/mainhub");

                // Default API path for other handlers
                // This allows for regular POST, GET, etc. API Calls
                //endpoints.MapControllerRoute(
                //    name: "default",
                //    pattern: "{controller=handler}/{action=Index}/{id?}");
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Backend Service Reached");
            });
        }
    }
}
